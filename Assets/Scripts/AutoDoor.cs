using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game
{
    public class AutoDoor : MonoBehaviour
    {
        [SerializeField] private float _doorDelay;
        [SerializeField] private GameObject _doorInside;


        [SerializeField] private LayerMask _playerMask;

        private float _timer = 0;
        private bool _closed = true;

        private void Awake()
        {
            _timer = _doorDelay;
        }

        private void Update()
        {
            if (Physics.Raycast(transform.position, Vector3.up, 1.5f, _playerMask))
                return;

            _timer -= Time.deltaTime;

            if (_timer <= 0)
            {
                _timer = _doorDelay;
                _closed = !_closed;

                Vector3 newPosition = transform.position;
                newPosition.y = _closed ? 0.5f:-1f; 
                transform.position = newPosition;
            }
        }

    }
}
