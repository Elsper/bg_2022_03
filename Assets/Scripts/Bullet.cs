using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private LayerMask _obstacleMask;

        private void Update()
        {
            Move();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out Player player))
                player.Kill();
        }

        private void Move()
        {
            float curentSteep = _speed * Time.deltaTime;

            var forwardRay = new Ray(transform.position, transform.up);
            if (Physics.Raycast(forwardRay, out RaycastHit hit, curentSteep, _obstacleMask))
            {
                Destroy(gameObject);
                return;
            }
            transform.Translate(transform.up * curentSteep, Space.World);
        }
    }
}