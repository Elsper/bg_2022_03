using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game
{
    public class Game : MonoBehaviour
    {
        [SerializeField] private int _currentLevelNumber = 1;
        [SerializeField] private int _maxLevel = 4;

        GameObject _currentLevelGO;

        [SerializeField] private GameObject _winGO;
        [SerializeField] private GameObject _loseGO;

        static Game Instance;

        private void Awake()
        {
            Instance = this;
            if (_currentLevelGO == null) RestartLevel();
        }

        public static void ShowLose()
        {
            Instance._loseGO.SetActive(true);
        }
        public static void ShowWin()
        {
            Instance._winGO.SetActive(true);
        }

        public void RestartLevel()
        {
            Instance._loseGO.SetActive(false);
            Instance._winGO.SetActive(false);

            if (_currentLevelGO!=null) Destroy(_currentLevelGO);

            _currentLevelGO = Instantiate(Resources.Load<GameObject>("level"+_currentLevelNumber));
        }

        public void NextLevel()
        {
            _currentLevelNumber++;
            if (_currentLevelNumber > _maxLevel) _currentLevelNumber = 1; //������ �� �����

            RestartLevel();
        }

    }
}
