﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
public class Level : MonoBehaviour
{
    [Header("Timer")]
    [SerializeField] private bool _timerIsOn;
    [SerializeField] private float _timerValue;

    [Header("Objects")]
    [SerializeField] private Player _player;
    [SerializeField] private Exit _exitFromLevel;
    
    private float _timer = 0;
    private bool _gameIsEnded = false;

    private void Awake()
    {
        _timer = _timerValue;
        _player.CurrentLevel=this;
    }

    private void Start()
    {
        _exitFromLevel.Close();
    }

    private void Update()
    {
        if(!_gameIsEnded)
        TimerTick();
    }

    private void TimerTick()
    {
        if(_timerIsOn == false)
            return;
        
        _timer -= Time.deltaTime;
        TimerText.SetTimerText = $"{_timer:F1}";
        
        if(_timer <= 0)
            Lose();
    }


    public void PlayerOnExit()
    {
        if(_exitFromLevel.IsOpen)
        Victory();
    }

    public void PlayerDeath()
    {
        Lose();
        Destroy(_player.gameObject);
    }

    public void PlayerFindKey()
    {
        _exitFromLevel.Open();
    }

    public void Victory()
    {
        _gameIsEnded = true;
        _player.Disable();

        Game.ShowWin();
    }

    public void Lose()
    {
        _gameIsEnded = true;
        _player.Disable();

        Game.ShowLose();
    }
}
}