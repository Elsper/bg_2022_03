﻿using UnityEngine;

namespace Game
{
[RequireComponent(typeof(Movement))]
public class Player : MonoBehaviour
{
    private Movement _movement;
    public Level CurrentLevel { get; set; }

    private void Awake()
    {
        _movement = GetComponent<Movement>();
    }

    private void Start()
    {
        Enable();
    }

    public void Enable()
    {
        _movement.enabled = true;
    }

    public void Disable()
    {
        _movement.enabled = false;
    }
        
    public void CheckExit()
    {
        CurrentLevel?.PlayerOnExit();
    }

    public void Kill()
    {
        CurrentLevel?.PlayerDeath();
    }

    public void PickUpKey()
    {
        CurrentLevel?.PlayerFindKey();
    }

    public void TeleportTo(Transform teleport)
    {
        transform.position = teleport.position;
    }
}
}