using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game
{
    public class Shooter : MonoBehaviour
    {
        [SerializeField] float _delayShoot = 2f;
        private float _timer = 0f;

        private void Update()
        {
            ShootTimerTick();
        }

        private void ShootTimerTick()
        {
            _timer -= Time.deltaTime;

            if (_timer <= 0)
            {
                MakeShoot();
                _timer = _delayShoot;
            }
        }

        void MakeShoot()
        {
            var bullet = Instantiate(Resources.Load<GameObject>("bullet"), transform.position, Quaternion.Euler(transform.rotation.eulerAngles+ new Vector3(90,0,0)), transform.parent);
            //�� �� ��������� ����� ���� ���������� ��������� 90 �������� �� ���� ��� ��������.
        }
    }
}
