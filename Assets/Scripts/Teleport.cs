using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game
{

    [RequireComponent(typeof(Collider))]
    public class Teleport : MonoBehaviour
    {
        [SerializeField] private GameObject _teleportOut;

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out Player player))
                player.TeleportTo(_teleportOut.transform);
        }

    }
}