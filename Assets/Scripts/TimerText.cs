using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Game
{
    public class TimerText : MonoBehaviour
    {
        private static Text _timerText;
        void Awake()
        {
            _timerText = this.gameObject.GetComponent<Text>();
        }

        public static string SetTimerText
        {
            set {
                if (_timerText == null) return;
                _timerText.text = value;
            }
        }
        
    }
}
